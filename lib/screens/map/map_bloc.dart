import 'dart:async';
import 'dart:math';

import 'package:coffeeradar/places_repository.dart';
import 'package:coffeeradar/screens/bloc_provider.dart';
import 'package:flutter/cupertino.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../resources.dart';
import '../selected_marker_manager.dart';
import 'markers/marker_creator.dart';

class MapBloc extends Bloc {
  StreamController<MapState> _stateController = StreamController<MapState>();

  Stream<MapState> get currentState => _stateController.stream;
  MapState _currentState = MapState.empty();
  Completer<GoogleMapController> _mapController = Completer();
  StreamSubscription _selectionSubscription;
  double _lastZoomLevel = -1;

  void start() {
    _updateState(MapState.empty());
    _selectionSubscription =
        SelectedMarkerManager().selectionUpdates.listen((event) {
      _updateState(_currentState.copyWith(
          selectedCafe: event.selectedCafe,
          selectedMarker: event.selectedMarker));
      _ensureCameraPositionCorrect();
    });
    _getCurrentLocation();
  }

  void _getCurrentLocation() async {
    Position res = await Geolocator().getCurrentPosition();
    final myPosition = LatLng(res.latitude, res.longitude);
    _updateState(_currentState.copyWith(myPosition: myPosition));
    _mapController.future.then((value) => {
          value.moveCamera(CameraUpdate.newCameraPosition(
              CameraPosition(target: myPosition, zoom: 15)))
        });
  }

  void _loadCafes(
      GoogleMapController controller, LatLng myPosition, double radius) async {
    final currentZoomLevel = await controller.getZoomLevel();
    if (currentZoomLevel == _lastZoomLevel) {
      return;
    }
    _lastZoomLevel = currentZoomLevel;
    final cafes = await fetchPlaces(myPosition, radius);
    final markers = await buildMarkers(cafes, _lastZoomLevel.toInt(), (id) {
      _onMarkerClicked(id);
    });
    _updateState(_currentState.copyWith(
        cafes: cafes, markers: markers, isLoading: false));
  }

  void _onMarkerClicked(String id) {
    final cafe =
        _currentState.cafes.firstWhere((cafe) => cafe.id == id, orElse: null);
    final selectedMarker = _currentState.markers
        .firstWhere((element) => element.markerId.value == cafe.id);
    _updateState(_currentState.copyWith(
        selectedCafe: cafe, selectedMarker: selectedMarker));
    _ensureCameraPositionCorrect();
  }

  void _updateState(MapState state) {
    _currentState = state;
    _stateController.sink.add(state);
  }

  void onMapReady(GoogleMapController mapController) {
    this._mapController.complete(mapController);
  }

  void onRouteClicked(FbCafe cafe) async {
    if (cafe == null) return;
    final url =
        "google.navigation:&q=${cafe.location.latitude},${cafe.location.longitude}&mode=w";
    if (await canLaunch(url)) {
      await launch(url);
    }
  }

  void onCloseDetailsClicked() {
    _updateState(_currentState.dropSelection());
  }

  void _ensureCameraPositionCorrect() async {
    if (!_currentState.markers.contains(_currentState.selectedMarker)) {
      final bounds =
          _calculateBounds(_currentState.myPosition, _currentState.selectedMarker.position);
      _mapController.future.then((value) =>
          {value.moveCamera(CameraUpdate.newLatLngBounds(bounds, 40))});
    }
  }

  LatLngBounds _calculateBounds(LatLng myPosition, LatLng markerPosition) {
    final ratio = 3;
    final firstLat = myPosition.latitude -
        (markerPosition.latitude - myPosition.latitude) * ratio;
    final firstLng = myPosition.longitude -
        (markerPosition.longitude - myPosition.longitude) * ratio;

    final secondLat = myPosition.latitude +
        (markerPosition.latitude - myPosition.latitude) * ratio;
    final secondLng = myPosition.longitude +
        (markerPosition.longitude - myPosition.longitude) * ratio;

    final southLat = min(firstLat, secondLat);
    final northLat = max(firstLat, secondLat);
    final westLng = min(firstLng, secondLng);
    final eastLng = max(firstLng, secondLng);

    return LatLngBounds(
        southwest: LatLng(southLat, westLng),
        northeast: LatLng(northLat, eastLng));
  }

  void onCameraIdle(BuildContext context) async {
    final controller = await _mapController.future;
    final myPosition = _currentState.myPosition;
    final radius = await _calculateSearchRadius(controller, myPosition, context);
    final circles = List<Circle>();
    circles.add(Circle(
      fillColor: CustomColors.mainAccent.withAlpha(40),
      strokeWidth: 1,
      strokeColor: CustomColors.mainAccent.withAlpha(80),
      circleId: CircleId("myself"),
      center: myPosition,
      radius: radius,
    ));
    _updateState(_currentState.copyWith(circles: circles));
    _loadCafes(controller, myPosition, radius);
  }

  Future<double> _calculateSearchRadius(GoogleMapController controller,
      LatLng myPosition, BuildContext context) async {
    final radius = _getCircleRadius(context);
    final myPositionOnScreen = await controller.getScreenCoordinate(myPosition);
    final radiusLatLng = await controller.getLatLng(ScreenCoordinate(
        x: (myPositionOnScreen.x + radius).toInt(), y: myPositionOnScreen.y));
    return Geolocator().distanceBetween(myPosition.latitude,
        myPosition.longitude, radiusLatLng.latitude, radiusLatLng.longitude);
  }

  double _getCircleRadius(BuildContext context) {
    const markerHorizontalMargin = 40;
    final double width = MediaQuery.of(context).size.width;
    return (width - markerHorizontalMargin * 2) * MediaQuery.of(context).devicePixelRatio / 2;
  }

  void dispose() {
    _selectionSubscription.cancel();
  }
}

class MapState {
  final List<FbCafe> cafes;
  final List<Marker> markers;
  final List<Circle> circles;
  final FbCafe selectedCafe;
  final Marker selectedMarker;
  final LatLng myPosition;
  final bool isLoading;

  MapState(
      {this.cafes,
      this.markers,
      this.circles,
      this.selectedMarker,
      this.selectedCafe,
      this.myPosition,
      this.isLoading});

  factory MapState.empty() {
    return MapState(
        markers: List(),
        cafes: List(),
        circles: List(),
        myPosition: LatLng(0, 0));
  }

  MapState copyWith(
      {List<FbCafe> cafes,
      List<Marker> markers,
      List<Circle> circles,
      Marker selectedMarker,
      FbCafe selectedCafe,
      LatLng myPosition,
      bool isLoading}) {
    return MapState(
      cafes: cafes ?? this.cafes,
      markers: markers ?? this.markers,
      circles: circles ?? this.circles,
      selectedMarker: selectedMarker ?? this.selectedMarker,
      selectedCafe: selectedCafe ?? this.selectedCafe,
      myPosition: myPosition ?? this.myPosition,
      isLoading: isLoading ?? this.isLoading,
    );
  }

  MapState dropSelection() {
    return MapState(
      cafes: cafes,
      markers: markers,
      circles: circles,
      selectedMarker: null,
      selectedCafe: null,
      myPosition: myPosition,
      isLoading: isLoading,
    );
  }
}
