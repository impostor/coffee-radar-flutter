
import 'dart:ui';

class CustomColors {
  static const textBlack = Color.fromARGB(255, 0, 8, 46);
  static const textGray = Color.fromARGB(255, 167, 174, 191);
  static const yellow = Color.fromARGB(255, 255, 176, 0);
  static const mainAccent = Color.fromARGB(255, 48, 74, 255);
  static const placeholder = Color.fromARGB(255, 215, 215, 215);
  static const placeholderAccent = Color.fromARGB(255, 170, 168, 168);
}