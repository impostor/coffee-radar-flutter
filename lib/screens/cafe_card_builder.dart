import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

import '../places_repository.dart';
import '../resources.dart';

class CardBuilder {
  static const _CARD_HEIGHT = 140.0;
  static const _CARD_PADDING = 12.0;
  static const _IMAGE_SIZE = _CARD_HEIGHT - _CARD_PADDING * 2;

  bool isClosable = false;
  Function onCloseTap;
  Function onRouteTap;

  CardBuilder({this.isClosable = false, this.onCloseTap, this.onRouteTap});

  Widget build(FbCafe cafe) {
    if (cafe == null) {
      return Container();
    }
    if (isClosable) {
      return _buildClosable(cafe);
    } else {
      return _buildCard(cafe, false);
    }
  }

  Widget _buildClosable(FbCafe cafe) {
    return SizedBox(
        height: _CARD_HEIGHT,
        child: Stack(
          children: <Widget>[
            _buildCard(cafe, true),
            Align(
                alignment: AlignmentDirectional.topEnd,
                child: _buildCloseButton()),
          ],
        ));
  }

  Widget _buildCloseButton() {
    return GestureDetector(
      child: Container(
        child: Icon(Icons.close, color: CustomColors.textGray),
        padding: EdgeInsets.all(8),
      ),
      onTap: () {
        onCloseTap();
      },
    );
  }

  Widget _buildCard(FbCafe cafe, bool routeButtonNeed) => Container(
      decoration: BoxDecoration(boxShadow: [
        BoxShadow(
            color: Color.fromARGB(85, 165, 180, 201),
            offset: Offset(0, 2),
            blurRadius: 6,
            spreadRadius: 2)
      ], color: Colors.white, borderRadius: BorderRadius.circular(12)),
      padding: EdgeInsets.all(_CARD_PADDING),
      height: _CARD_HEIGHT,
      child:
          Row(crossAxisAlignment: CrossAxisAlignment.center, children: <Widget>[
        _buildImage(cafe.cover?.source),
        SizedBox(width: 12),
        Expanded(
            child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            _buildTitle(cafe.title),
            SizedBox(height: 8),
            _buildAddress(cafe),
            Spacer(),
            _buildFooter(cafe.rating, cafe.distance.toInt().toString() + "m",
                routeButtonNeed)
          ],
        ))
      ]));

  Widget _buildImage(String url) {
    final imageProvider =
        Image.asset('assets/ic_drink.png', width: 36, height: 36);
    final double imageSize = _IMAGE_SIZE;
    Widget image;
    if (url == null) {
      image = Container(
          decoration: BoxDecoration(color: CustomColors.placeholder),
          height: imageSize,
          width: imageSize,
          alignment: Alignment.center,
          child: imageProvider);
    } else {
      image = CachedNetworkImage(
          imageUrl: url,
          height: imageSize,
          width: imageSize,
          fit: BoxFit.cover,
          placeholder: (context, url) => Container(
                height: imageSize,
                decoration: BoxDecoration(color: CustomColors.placeholder),
                alignment: Alignment.center,
                child: Container(
                  height: 24,
                  width: 24,
                  child: CircularProgressIndicator(
                    valueColor: new AlwaysStoppedAnimation<Color>(
                        CustomColors.placeholderAccent),
                  ),
                ),
              ));
    }
    return Container(
      width: imageSize,
      child: ClipRRect(
        child: image,
        borderRadius: BorderRadius.circular(8),
      ),
    );
  }

  Widget _buildTitle(String title) {
    final double rightMargin = isClosable ? 28 : 0;
    return Container(
        padding: EdgeInsets.only(right: rightMargin),
        child: Text(
          title,
          textAlign: TextAlign.start,
          maxLines: 2,
          style: TextStyle(
              fontFamily: 'Montserrat',
              fontWeight: FontWeight.w700,
              fontSize: 14,
              color: CustomColors.textBlack),
        ));
  }

  Widget _buildAddress(FbCafe cafe) {
    final double rightMargin = isClosable ? 28 : 0;
    String address = cafe.formattedAddress();
    return Container(
        padding: EdgeInsets.only(right: rightMargin),
        child: RichText(
            maxLines: 2,
            overflow: TextOverflow.ellipsis,
            text: TextSpan(children: [
              WidgetSpan(
                alignment: PlaceholderAlignment.middle,
                child: Icon(
                  Icons.location_on,
                  size: 12,
                  color: CustomColors.mainAccent,
                ),
              ),
              WidgetSpan(child: SizedBox(width: 2)),
              TextSpan(
                  text: address,
                  style: TextStyle(color: CustomColors.textGray, fontSize: 12))
            ])));
  }

  Widget _buildFooter(double rate, String distance, bool buttonNeed) {
    Widget leftWidget;
    if (rate != null) {
      leftWidget = RichText(
          maxLines: 2,
          overflow: TextOverflow.ellipsis,
          text: TextSpan(children: [
            WidgetSpan(
              alignment: PlaceholderAlignment.middle,
              child: Icon(
                Icons.star,
                size: 14,
                color: CustomColors.yellow,
              ),
            ),
            WidgetSpan(child: SizedBox(width: 4)),
            TextSpan(
                text: rate.toString(),
                style: TextStyle(
                    color: CustomColors.textBlack,
                    fontWeight: FontWeight.w500,
                    fontSize: 14))
          ]));
    } else {
      leftWidget = Spacer();
    }
    Widget rightWidget = buttonNeed
        ? buildRouteButton(distance)
        : Text(
            distance,
            textAlign: TextAlign.start,
            style: TextStyle(
                fontWeight: FontWeight.w400,
                fontSize: 12,
                color: CustomColors.textGray),
          );
    return Row(
      children: <Widget>[leftWidget, Spacer(), rightWidget],
    );
  }

  Widget buildRouteButton(String distance) {
    final icon = Image.asset(
      'assets/start.png',
      width: 24,
      height: 24,
      color: Colors.white,
    );
    return GestureDetector(
      child: Container(
        height: 38,
        padding: EdgeInsets.only(left: 12, right: 12),
        decoration: BoxDecoration(
            color: CustomColors.mainAccent,
            borderRadius: BorderRadius.circular(8)),
        child: Center(
            child: RichText(
                maxLines: 2,
                overflow: TextOverflow.ellipsis,
                text: TextSpan(children: [
                  WidgetSpan(
                      alignment: PlaceholderAlignment.middle, child: icon),
                  WidgetSpan(child: SizedBox(width: 4)),
                  TextSpan(
                      text: distance,
                      style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.w400,
                          fontSize: 12))
                ]))),
      ),
      onTap: () {
        onRouteTap?.call();
      },
    );
  }
}
