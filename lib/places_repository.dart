
import 'dart:convert';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:http/http.dart' as http;
import 'dart:developer' as developer;

Future<List<FbCafe>> fetchPlaces(LatLng currentLocation, double radius) async {
  return fetchPlacesByQuery(currentLocation, radius, "cafe");
}

Future<List<FbCafe>> fetchPlacesByQuery(LatLng currentLocation, double radius, String query) async {
  String queryParam = "q=$query";
  String centerParam = "center=${currentLocation.latitude},${currentLocation.longitude}";
  String distanceParam = "distance=$radius";
  String accessTokenParam = "access_token=235956027829092|d7478584f795d3407e22bbabd2400546";
  String fieldsParam = "fields=name,cover,link,description,location,overall_star_rating";
  final response = await http.get('https://graph.facebook.com/search?type=place&$queryParam&$accessTokenParam&$fieldsParam&$centerParam&$distanceParam');
  if (response.statusCode == 200) {
    Geolocator geolocator = Geolocator();
    developer.log("RESPONSE: ${response.body}");
    List<FbCafe> cafes = FbResponse.fromJson(json.decode(response.body)).data;
    for (var element in cafes) {
      element.distance = await geolocator.distanceBetween(currentLocation.latitude, currentLocation.longitude, element.location.latitude, element.location.longitude);
    }

    cafes.sort(((a, b) { return a.distance.compareTo(b.distance); }));
    return cafes;
  } else {
    throw Exception('Failed to load cafes');
  }
}

class FbResponse {
  final List<FbCafe> data;
  FbResponse({this.data});
  factory FbResponse.fromJson(Map<String, dynamic> json) {
    List data = json["data"] as List;
    List<FbCafe> result = data.map<FbCafe>((json) => FbCafe.fromJson(json)).toList();
    return FbResponse(data: result);
  }
}

class FbCafe {
  final String id;
  final String title;
  final String description;
  final double rating;
  final FbCover cover;
  final FbLocation location;
  double distance = 0.0;

  FbCafe(
      {this.id,
      this.title,
      this.cover,
      this.location,
      this.description,
      this.rating});

  String formattedAddress() {
    if (isNotNullOrEmpty(location.street)) {
      return location.street;
    } else if (isNotNullOrEmpty(location.city)) {
      return location.city;
    } else if (isNotNullOrEmpty(location.state)) {
      return location.state;
    } else {
      return location.country;
    }
  }

  bool isNotNullOrEmpty(String arg) {
    return arg != null && arg.isNotEmpty;
  }

  factory FbCafe.fromJson(Map<String, dynamic> json) {
    FbCover cover;
    if (json['cover'] != null) {
      cover = FbCover.fromJson(json['cover']);
    }
    return FbCafe(
        id: json['id'],
        title: json['name'],
        description: json['description'],
        rating: json['overall_star_rating']?.toDouble(),
        cover: cover,
        location: FbLocation.fromJson(json['location']));
  }
}

class FbLocation {
  final String city;
  final String country;
  final String street;
  final String state;
  final double latitude;
  final double longitude;

  FbLocation(
      {this.city,
      this.country,
      this.street,
      this.state,
      this.latitude,
      this.longitude});

  factory FbLocation.fromJson(Map<String, dynamic> json) {
    return FbLocation(
        city: json['city'],
        country: json['country'],
        street: json['street'],
        state: json['state'],
        latitude: json['latitude']?.toDouble(),
        longitude: json['longitude']?.toDouble());
  }
}

class FbCover {
  final String source;

  FbCover({this.source});

  factory FbCover.fromJson(Map<String, dynamic> json) {
    return FbCover(source: json['source']);
  }
}
