

import 'package:flutter/cupertino.dart';

class BlocProvider extends StatefulWidget {

  final Bloc bloc;
  final Widget child;

  BlocProvider({Key key, this.bloc, this.child}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _BlocState(bloc: bloc, child: child);
  }

}

class _BlocState extends State<BlocProvider> {

  final Bloc bloc;
  final Widget child;

  _BlocState({this.bloc, this.child}) : super();

  @override
  Widget build(BuildContext context) {
    return child;
  }

  @override
  void dispose() {
    bloc?.dispose();
    super.dispose();
  }
}

abstract class Bloc {
  void dispose() {}
}
