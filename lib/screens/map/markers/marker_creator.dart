
import 'package:google_maps_flutter/google_maps_flutter.dart';

import '../../../places_repository.dart';
import 'cafe_marker_creator.dart';
import 'cluster_maker.dart';

Future<List<Marker>> buildMarkers(List<FbCafe> cafes, int currentZoom, void Function(String) onTap) async {
  final markers = await createCafeMarkersList(cafes, onTap);
  final fluster = await createFluster(markers, onTap);
  final clusters = fluster.clusters([-180, -85, 180, 85], currentZoom);

  List<Marker> result = List();

  final pinPlaceholder = await buildPlaceholderPin();

  for (MapMarker marker in clusters) {
    BitmapDescriptor bitmapDescriptor;
    if (marker.isCluster) {
      bitmapDescriptor = await makeClusterImage(marker.pointsSize);
    } else {
      bitmapDescriptor = await buildMarkerImage(marker.image, pinPlaceholder);
    }
    result.add(marker.toMarker(bitmapDescriptor));
  }

  return result;
}