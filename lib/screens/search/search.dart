import 'dart:async';

import 'package:coffeeradar/places_repository.dart';
import 'package:coffeeradar/resources.dart';
import 'package:coffeeradar/screens/search/search_cafes_usecase.dart';
import 'package:flutter/material.dart';
import 'dart:developer' as developer;

import 'package:google_maps_flutter/google_maps_flutter.dart';

import '../cafe_card_builder.dart';
import '../selected_marker_manager.dart';

class DataSearchDelegate extends SearchDelegate<String> {

  LatLng myPosition;

  DataSearchDelegate({this.myPosition}) : super();

  @override
  List<Widget> buildActions(BuildContext context) {
    return [
      IconButton(
        icon: Icon(Icons.clear),
        onPressed: () {
          query = "";
        },
      )
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    return IconButton(
      icon: AnimatedIcon(
          icon: AnimatedIcons.menu_arrow, progress: transitionAnimation),
      onPressed: () {
        close(context, null);
      },
    );
  }

  @override
  Widget buildResults(BuildContext context) {
    return Container();
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    developer.log("buildSuggestions $query");
    final messageTextStyle = TextStyle(
        fontWeight: FontWeight.w500,
        fontSize: 14,
        color: CustomColors.placeholderAccent
    );
    if (query.isEmpty) {
      return Center(child: Text("Начните вводить название...", style: messageTextStyle));
    }
    return FutureBuilder<SearchState>(
      initialData: SearchState(isLoading: true),
      future: loadData(query),
      builder: (context, snapshot) {
        if (snapshot.error != null) {
          return Center(child: Text("Произошла ошибка", style: messageTextStyle));
        } else if (!snapshot.hasData || snapshot.data.isLoading) {
          return Center(child: CircularProgressIndicator(valueColor: new AlwaysStoppedAnimation<Color>(CustomColors.mainAccent)));
        } else {
          List<FbCafe> cafes = snapshot.data.data;
          final cardBuilder = CardBuilder();
          return ListView.builder(
              itemCount: cafes.length,
              itemBuilder: (BuildContext ctxt, int index) {
                final cardWidget =  cardBuilder.build(cafes[index]);
                final paddings = EdgeInsets.only(left: 12, right: 12, top: index == 0 ? 12 : 6, bottom: index == cafes.length - 1 ? 12 : 6);
                return GestureDetector(
                    onTap: () {
                      SelectedMarkerManager().onCafeSelected(cafes[index], null);
                      Navigator.pop(context);
//              showDetails(cafe);
                    },
                    child: Padding(child: cardWidget, padding: paddings)
                );
              });
        }
      },
    );
  }

  Future<SearchState> loadData(String query) async {
    final cafes = await searchPlaces(myPosition, query);
    return SearchState(data: cafes);
  }

  @override
  ThemeData appBarTheme(BuildContext context) {
    final ThemeData theme = Theme.of(context);
    return theme.copyWith(
        inputDecorationTheme: theme.inputDecorationTheme.copyWith(
            hintStyle: theme.textTheme.subtitle1.copyWith(
          color: Colors.white70,
          fontWeight: FontWeight.w400,
        )),
        primaryColor: theme.primaryColor,
        primaryIconTheme: theme.primaryIconTheme,
        primaryColorBrightness: theme.primaryColorBrightness,
        textTheme: theme.textTheme.copyWith(title: theme.textTheme.title.copyWith(color: Colors.white))
    );
  }
}

class SearchState {
  bool isLoading;
  bool isError;
  List<FbCafe> data;

  SearchState({this.isLoading = false, this.isError = false, this.data});
}