
import 'package:coffeeradar/screens/bloc_provider.dart';
import 'package:coffeeradar/screens/cafe_card_builder.dart';
import 'package:coffeeradar/places_repository.dart';
import 'package:coffeeradar/resources.dart';
import 'package:coffeeradar/screens/map/map_bloc.dart';
import 'package:coffeeradar/screens/search/search.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Coffee Radar',
      theme: ThemeData(
        primaryColor: CustomColors.mainAccent,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MapPage(),
    );
  }
}

class MapPage extends StatelessWidget {

  final MapBloc _bloc = MapBloc();

  @override
  Widget build(BuildContext context) {
    _bloc.start();
    return BlocProvider(
      bloc: _bloc,
      child: StreamBuilder<MapState>(
          initialData: MapState.empty(),
          stream: _bloc.currentState,
          builder: (context, snapshot) {
            final state = snapshot.data;
            return Scaffold(
              body: Center(
                child: Stack(
                  children: <Widget>[
                    _buildMap(context, state),
                    Align(
                        alignment: AlignmentDirectional.bottomCenter,
                        child: _buildBody(state.selectedCafe)),
                    Align(alignment: AlignmentDirectional.topEnd, child: _searchButton(context, state.myPosition))
                  ],
                ),
              ),
            );
          }
      ),

    );
  }

  Widget _searchButton(BuildContext context, LatLng myPosition) => Container(
    child: IconButton(
        icon: Icon(
          Icons.search,
          color: CustomColors.mainAccent,
        ),
        onPressed: () {
          showSearch(
              context: context,
              delegate: DataSearchDelegate(myPosition: myPosition));
        }),
    margin: EdgeInsets.only(top: 27, right: 12),
  );

  Widget _buildMap(BuildContext context, MapState state) {
    CameraPosition myLocation = CameraPosition(target: state.myPosition, zoom: 12);
    final markers = List<Marker>();
    if (state.selectedCafe != null) {
      markers.add(state.selectedMarker);
    } else {
      markers.addAll(state.markers);
    }
    return GoogleMap(
      mapToolbarEnabled: false,
      myLocationEnabled: true,
      myLocationButtonEnabled: false,
      initialCameraPosition: myLocation,
      mapType: MapType.normal,
      zoomControlsEnabled: false,
      markers: Set<Marker>.of(markers),
      circles: Set.of(state.circles ?? List()),
      onMapCreated: (GoogleMapController controller) {
        _bloc.onMapReady(controller);
      },
      onCameraIdle: () {
        _bloc.onCameraIdle(context);
      },
    );
  }

  Widget _buildBody(FbCafe selectedCafe) {
    return Container(
        margin: const EdgeInsets.only(bottom: 32.0),
        child: _buildBottomContent(selectedCafe)
    );
  }

  Widget _buildBottomContent(FbCafe selectedCafe) {
    final cardBuilder = CardBuilder(
        isClosable: true,
        onCloseTap: () {
          _bloc.onCloseDetailsClicked();
        },
        onRouteTap: () {
          if (selectedCafe == null) return;
          _bloc.onRouteClicked(selectedCafe);
        });

    return AnimatedOpacity(
        opacity: selectedCafe != null ? 1.0 : 0.0,
        duration: Duration(milliseconds: 300),
        curve: Curves.easeInToLinear,
        child: Container(
            padding: EdgeInsets.only(left: 16, right: 16, top: 8, bottom: 8),
            child: cardBuilder.build(selectedCafe)
        )
    );
  }
}