import 'dart:io';
import 'dart:typed_data';
import 'dart:ui';
import 'dart:ui' as ui show Image;

import 'package:coffeeradar/places_repository.dart';
import 'package:coffeeradar/resources.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';

import 'cluster_maker.dart';

Future<List<MapMarker>> createCafeMarkersList(List<FbCafe> cafes, void Function(String) onTap) async {
  List<MapMarker> markers = List();
  for (var cafe in cafes) {
     markers.add(await _createMarker(cafe, onTap));
  }
  return markers;
}

Future<MapMarker> _createMarker(FbCafe cafe, void Function(String) onTap) async {
  return MapMarker(
    id: cafe.id,
    position: LatLng(
      cafe.location.latitude,
      cafe.location.longitude,
    ),
    isCluster: false,
    image: cafe.cover?.source,
    onTap: onTap
  );
}

Future<BitmapDescriptor> buildMarkerImage(String imageUrl, ui.Image placeholder) async {
  final circleAvatar = imageUrl != null ? await _buildCircleAvatar(imageUrl, 90) : placeholder;
  final markerPin = await _buildMarkerPin(circleAvatar);

  return toBitmapDescriptor(markerPin);
}

Future<ui.Image> _buildCircleAvatar(String imageUrl, int targetWidth) async {
  final File markerImageFile =
      await DefaultCacheManager().getSingleFile(imageUrl);
  final Uint8List markerImageBytes = await markerImageFile.readAsBytes();
  final originalImage = await decodeImageFromList(markerImageBytes);
  return await _cropAndScaleImage(originalImage, targetWidth);
}

Future<ui.Image> _cropAndScaleImage(ui.Image originalImage, int targetWidth) {
  double size;
  if (originalImage.width <= originalImage.height) {
    size = originalImage.width.toDouble();
  } else {
    size = originalImage.height.toDouble();
  }
  Rect src = Rect.fromCenter(
      center: Offset(originalImage.width / 2, originalImage.height / 2),
      width: size,
      height: size);
  Rect dst = Rect.fromLTRB(0, 0, size, size);
  Path path = Path()
    ..addOval(Rect.fromLTWH(0, 0, size.toDouble(), size.toDouble()));

  var pictureRecorder = PictureRecorder();
  Canvas canvas = new Canvas(pictureRecorder);
  canvas.scale(targetWidth / size);
  canvas.clipPath(path);
  canvas.drawColor(CustomColors.mainAccent, BlendMode.color);
  canvas.drawImageRect(originalImage, src, dst, Paint());
  return pictureRecorder
      .endRecording()
      .toImage(targetWidth.floor(), targetWidth.floor());
}

Future<ui.Image> _buildMarkerPin(ui.Image avatar) async {
  final ByteData markerPin = await rootBundle.load('assets/ic_pin_bg.png');
  final pinImage = await decodeImageFromList(markerPin.buffer.asUint8List());

  final offset = (pinImage.width - avatar.width) / 2;

  final imagePaint = Paint();
  imagePaint.color = CustomColors.mainAccent;

  var pictureRecorder = PictureRecorder();
  Canvas canvas = new Canvas(pictureRecorder);
  canvas.drawImage(pinImage, Offset.zero, imagePaint);
  canvas.drawImage(avatar, Offset(offset, offset), Paint());
  return await pictureRecorder
      .endRecording()
      .toImage(pinImage.width, pinImage.height);
}

Future<ui.Image> buildPlaceholderPin() async {
  final ByteData markerPin = await rootBundle.load('assets/ic_pin_placeholder.png');
  return await decodeImageFromList(markerPin.buffer.asUint8List());
}

Future<BitmapDescriptor> toBitmapDescriptor(ui.Image image) async {
  final ByteData byteData = await image.toByteData(format: ImageByteFormat.png);
  final Uint8List resizedMarkerImageBytes = byteData.buffer.asUint8List();
  return BitmapDescriptor.fromBytes(resizedMarkerImageBytes);
}