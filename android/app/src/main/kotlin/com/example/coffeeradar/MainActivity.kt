package com.example.coffeeradar

import io.flutter.embedding.android.FlutterActivity

class MainActivity: FlutterActivity() {

    override fun onDestroy() {
        flutterEngine?.platformViewsController?.onFlutterViewDestroyed()
        super.onDestroy()
    }
}
