import 'dart:async';

import 'package:coffeeradar/places_repository.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

import 'map/markers/cafe_marker_creator.dart';

class SelectedMarkerManager {
  static final SelectedMarkerManager _instance =
      SelectedMarkerManager._internal();

  factory SelectedMarkerManager() {
    return _instance;
  }

  SelectedMarkerManager._internal();

  final StreamController _controller = StreamController<SelectedData>();
  Stream<SelectedData> get selectionUpdates => _controller.stream;

  void onCafeSelected(FbCafe selectedCafe, Marker selectedMarker) {
    if (selectedMarker == null) {
      _buildMarker(selectedCafe).then((value) => {
            _controller.add(SelectedData(selectedCafe: selectedCafe, selectedMarker: value))
          });
    } else {
      _controller.add(SelectedData(
          selectedCafe: selectedCafe, selectedMarker: selectedMarker));
    }
  }

  Future<Marker> _buildMarker(FbCafe cafe) async {
    final pinPlaceholder = await buildPlaceholderPin();
    final bitmapDescriptor =
        await buildMarkerImage(cafe.cover?.source, pinPlaceholder);
    return Marker(
        markerId: MarkerId(cafe.id),
        position: LatLng(
          cafe.location.latitude,
          cafe.location.longitude,
        ),
        icon: bitmapDescriptor,
        consumeTapEvents: true);
  }
}

class SelectedData {
  final Marker selectedMarker;
  final FbCafe selectedCafe;

  SelectedData({this.selectedMarker, this.selectedCafe});
}
