import 'dart:typed_data';
import 'dart:ui';

import 'package:fluster/fluster.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

import 'cafe_marker_creator.dart';

// inspired by https://medium.com/coletiv-stories/how-to-cluster-markers-on-flutter-google-maps-44620f607de3
// https://medium.com/jet-set-digital/map-marker-clustering-flutter-ft-google-maps-fluster-e9fe4e2c503b

Future<Fluster<MapMarker>> createFluster(List<MapMarker> markers, void Function(String) onTap) async {
  return Fluster<MapMarker>(
      minZoom: 0,
      maxZoom: 20,
      radius: 150,
      extent: 2048,
      nodeSize: 64,
      points: markers,
      createCluster: (
        // Create cluster marker
        BaseCluster cluster,
        double lng,
        double lat,
      ) {
        return MapMarker(
          id: cluster.id.toString(),
          position: LatLng(lat, lng),
          isCluster: cluster.isCluster,
          clusterId: cluster.id,
          pointsSize: cluster.pointsSize,
          childMarkerId: cluster.childMarkerId,
          onTap: onTap
        );
      });
}

Future<BitmapDescriptor> makeClusterImage(int count) async {
  final ByteData clusterBackgroundBytes =
      await rootBundle.load('assets/ic_cluster.png');
  final clusterBackground =
      await decodeImageFromList(clusterBackgroundBytes.buffer.asUint8List());

  var pictureRecorder = PictureRecorder();
  Canvas canvas = new Canvas(pictureRecorder);
  canvas.drawImage(clusterBackground, Offset.zero, Paint());

  final textStyle = TextStyle(
    color: Colors.white,
    fontSize: 40,
  );
  final textSpan = TextSpan(
    text: '$count',
    style: textStyle,
  );
  final textPainter = TextPainter(
    text: textSpan,
    textAlign: TextAlign.center,
    textDirection: TextDirection.ltr,
  );
  textPainter.layout(
    minWidth: 0,
    maxWidth: clusterBackground.width - 40.toDouble(),
  );
  final offset = Offset((clusterBackground.width - textPainter.width) / 2,
      (clusterBackground.height - textPainter.height) / 2);
  textPainter.paint(canvas, offset);

  final image = await pictureRecorder
      .endRecording()
      .toImage(clusterBackground.width, clusterBackground.height);
  return toBitmapDescriptor(image);
}

class MapMarker extends Clusterable {
  final String id;
  final LatLng position;
  final void Function(String) onTap;
  final String image;

  MapMarker({
    @required this.id,
    @required this.position,
    isCluster = false,
    clusterId,
    pointsSize,
    childMarkerId,
    this.image,
    this.onTap,
  }) : super(
          markerId: id,
          latitude: position.latitude,
          longitude: position.longitude,
          isCluster: isCluster,
          clusterId: clusterId,
          pointsSize: pointsSize,
          childMarkerId: childMarkerId,
        );

  Marker toMarker(BitmapDescriptor icon) => Marker(
      markerId: MarkerId(id),
      position: LatLng(
        position.latitude,
        position.longitude,
      ),
      icon: icon,
      consumeTapEvents: true,
      onTap: () {
        this.onTap(this.id);
      });
}
