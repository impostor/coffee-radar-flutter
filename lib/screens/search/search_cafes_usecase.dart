

import 'package:google_maps_flutter/google_maps_flutter.dart';

import '../../places_repository.dart';

Future<List<FbCafe>> searchPlaces(LatLng currentLocation, String query) async {
  return fetchPlacesByQuery(currentLocation, 5000, "$query");
}